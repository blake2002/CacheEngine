﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ha666.Redis;

namespace CacheEngine.Redis
{
    public class RedisCacheEngine : ICacheEngine
    {
        /// <summary>根据关键字取得缓存数据</summary>
        /// <param name="key">关键字</param>
        /// <returns>缓存数据</returns>
        public T Get<T>(string key)
        {
            ProtobufKey pkey = new ProtobufKey(key);
            CachedItem cachedItem = null;
            cachedItem = pkey.Get<CachedItem>(0);
            object obj = null;
            if (cachedItem != null)
            {
                if (cachedItem.ExpirationDate > DateTime.Now)
                {
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(cachedItem.Item);
                }
                else
                {
                    pkey.Delete(0);
                }
            }
            return (T)obj;
    
        }
        /// <summary>保存缓存数据</summary>
        /// <param name="key">关键字</param>
        /// <param name="itemToCache">缓存数据</param>
        /// <param name="expirationDate">过期时间</param>
        public void SaveOrUpdate(string key, object itemToCache, DateTime expirationDate)
        {
            ProtobufKey set = new ProtobufKey(key);

            TimeSpan ts = new TimeSpan();
            ts = (expirationDate - DateTime.Now);
            var ci= new CachedItem();
            string ser = Newtonsoft.Json.JsonConvert.SerializeObject(itemToCache);
            ci.Item= ser;
            ci.ExpirationDate= expirationDate;
            set.Set(0, ci);//, ts.Seconds, ts.Milliseconds, false);
          //  set.Expire(0, (long)ts.TotalSeconds);
        }

    }
}
