﻿using Memcached.ClientLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CacheEngine.Memcache
{
    /// <summary>基于Memcache的缓存</summary>
    public class MemcacheCacheEngine : ICacheEngine
    {
        private readonly MemcachedClient _cacheClient;

        /// <summary>默认构造</summary>
        /// <param name="cacheClient">MemcachedClient</param>
        public MemcacheCacheEngine(MemcachedClient cacheClient)
        {
            if (null == cacheClient)
                throw new ArgumentNullException("must provide a sockIoPool");
            _cacheClient = cacheClient;
        }
        /// <summary>根据关键字取得缓存数据</summary>
        /// <param name="key">关键字</param>
        /// <returns>缓存数据</returns>
        public T Get<T>(string key)
        {
            return (T)_cacheClient.Get(key);
        }

        /// <summary>保存缓存数据</summary>
        /// <param name="key">关键字</param>
        /// <param name="itemToCache">缓存数据</param>
        /// <param name="expirationDate">过期时间</param>
        public void SaveOrUpdate(string key, object itemToCache, DateTime expirationDate)
        {
            _cacheClient.Set(key, itemToCache, expirationDate);
        }
    }
}
